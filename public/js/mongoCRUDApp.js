/**
 * Here we create a module that matches the ng-app attribute in <html>
 *     and configure it to load partial html files that
 *     goes into the ng-view view with a certain controller (notice that the
 *     partial html files detail.html and list.html don't have ng-controller in them.
 * @type {*}
 */
var MyApp = angular.module('mongoCRUD', []).
    config(function($routeProvider) {
        $routeProvider.
            when( '/',          { templateUrl: 'list.html',   controller: "ItemsCtrl" } ).
            when( '/item/:id',  { templateUrl: 'detail.html', controller: "ItemCtrl" } ).
            when( '/new',       { templateUrl: 'detail.html', controller: "AddCtrl" })
    });

/**
 * This is a service (effectively a singleton) that can be used anywhere.
 * This one accepts alerts which will be displayed on index.html before the
 * routed html.
 */
MyApp.service('alertService', function() {
    var alerts = [];
    this.addAlert = function(text) {
        alerts.push(text);
    };
    this.closeAlert = function(index) {
        alerts.splice(index, 1);
    };
    this.getAlerts = function() {
        return alerts;
    };
});
