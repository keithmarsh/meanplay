var MyApp = angular.module('mongoCRUD');

/**
 * The AlertsController uses the alertService to get alerts into the scope.
 * Is there a better way to do this?
 */
MyApp.controller('AlertsCtrl', function AlertsCtrl($scope, alertService) {
    $scope.alerts = alertService.getAlerts();
    $scope.closeAlert = function(index) {
        alertService.closeAlert(index);
    }
});

/**
 * The items controller fetches the list from the server api and puts the answer in the scope.
 */
MyApp.controller('ItemsCtrl', function ItemsCtrl($scope, $http, alertService) {
    $http.get('/api/items')
        .success(function(data) {
            $scope.items = data;
        })
        .error(function(data, status) {
            console.log("$http.get error " + status + " : " + data);
            alertService.addAlert(data);
        });
});

/**
 * The item (singular) controller gets a record from the server api and puts it in the scope.
 * It also handles updates and deletion requests by calling the server api and on success
 * jumping back to the list route /
 */
MyApp.controller('ItemCtrl', function ItemCtrl($scope, $http, $routeParams, $location, alertService) {
    $http.get('/api/item/'+$routeParams.id)
        .success(function(data) {
           $scope.item = data;
        })
        .error(function(data, status) {
            console.log("$http.get error " + status + " : " + data);
            alertService.addAlert(data);
        });
    $scope.save = function() {
        $http.put('/api/item/' + $scope.item._id, {
            "_id": $scope.item._id,
            "name": $scope.item.name,
            "site": $scope.item.site,
            "description" : $scope.item.description } )
            .success(function(data) {
                $location.path('/');
            })
            .error(function(data, status) {
                console.log("$http.put error " + status + " : " + data);
                alertService.addAlert(data);
            });
    }; // save
    $scope.destroy = function() {
        $http.delete('/api/item/' + $scope.item._id)
        .success(function(data) {
            $location.path('/');
        })
        .error(function(data, status) {
            console.log("$http.delete error " + status + " : " + data);
            alertService.addAlert(data);
        });
    }; // destroy
});

/**
 * The Add Controller inserts the form content to the server api and jumps back to the
 * list route / when complete.
 */
MyApp.controller('AddCtrl', function AddCtrl($scope, $http, $location, alertService) {
    $scope.save = function() {
        $http.post('/api/items', {
            "name": $scope.item.name,
            "site": $scope.item.site,
            "description" : $scope.item.description } )
        .success(function(data) { //}, status, headers, config) {
            $location.path('/');
        })
        .error(function(data, status) {
            console.log("$http.post error " + status + " : " + data);
            alertService.addAlert(data);
        });
    }; // save
});
