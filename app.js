var express = require('express');
var api = require('./routes/api');
var path = require('path');
var app = express();

//I haven't used middleware renderers.  No need here, we'resending data to the Angular,
// and it's doing it all.  No Jade or EJS must save a packet.  The static handler is
// doing all the work.
app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080);
app.set('address', process.env.OPENSHIFT_NODEJS_IP || process.env.IP || "127.0.0.1");
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.favicon());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router); // If this is missing, the error handler below isn't used
app.use(function(err, req, res, next) {
    res.send(500, err.name + ": " + err.errmsg);
    console.log(JSON.stringify(err));
});

// RESTful Routes.
app.get('/api/items', api.list);
app.get('/api/item/:item_id', api.read);
app.post('/api/items', api.create);
app.put('/api/item/:item_id', api.update);
app.delete('/api/item/:item_id', api.delete);

app.listen(app.get('port'), app.get('address'), function() {
    console.log("Express server listening on " + app.get('address') + ":" + app.get('port') + " in mode: " + app.settings.env);
});