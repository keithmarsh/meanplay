var mongo = require("mongodb");
var dbURL = "mongodb://guest:guest@ds037368.mongolab.com:37368/meanplay";
var collName = "items";

/**
 * CRUD List all items
 */
exports.list = function(req, res, next) {
    mongo.MongoClient.connect(dbURL, function (err, db) {
        if (err !== null) { next(err); return; }
        db.collection(collName, function (err, items) {
            if (err !== null) { db.close(); next(err); return; }
            items.find( {}, function (err,cursor) {
                if (err !== null) { db.close(); next(err); return; }
                cursor.toArray( function (err, arr) {
                    if (err !== null) { db.close(); next(err); return; }
                    res.json(arr);
                    db.close();
                }); // toArray
            }); // find
        }); // collection
    }); // connect
};

/**
 * cRUD Create an item
 */
exports.create = function(req, res, next) {
    mongo.MongoClient.connect(dbURL, function (err, db) {
        if (err !== null) { next(err); return; }
        db.collection(collName, function (err, items) {
            if (err !== null) { db.close(); next(err); return; }
            items.insert(req.body, {"safe":true}, function(err, records) {
                if (err !== null) { db.close(); next(err); return; }
                db.close();
                res.status(200).send(records[0]);
            });
        });
    }); // connect
};

/**
 * CrUD Read an item. Key is request param item_id
 */
exports.read = function(req, res, next) {
    mongo.MongoClient.connect(dbURL, function (err, db) {
        if (err !== null) { next(err); return; }
        db.collection(collName, function (err, items) {
            if (err !== null) { db.close(); next(err); return; }
            items.findOne({"_id":new mongo.ObjectID(req.params.item_id)}, function (err,item) {
                if (err !== null) { db.close(); next(err); return; }
                console.log(req.params.item_id + " " + JSON.stringify(item));
                db.close();
                res.json(item);
            }); // find
        }); // collection
    }); // connect
};

/**
 * CRuD Update an item.  Key is request param _id
 */
exports.update = function(req, res, next) {
    mongo.MongoClient.connect(dbURL, function (err, db) {
        if (err !== null) { next(err); return; }
        db.collection(collName, function (err, items) {
            if (err !== null) { db.close(); next(err); return; }
            items.update({"_id":new mongo.ObjectID(req.body._id)}, req.body, {"safe":true, "multi":false, "upsert":false}, function(err, count) {
                if (err !== null) { db.close(); next(err); return; }
                db.close();
                res.status(200).json({'count':count});
            }); // update
        }); // collection
    }); // connect
};

/**
 * CRUd Delete an item.  Key is request param item_id
 */
exports.delete = function(req, res, next) {
    mongo.MongoClient.connect(dbURL, function (err, db) {
        if (err !== null) { next(err); return; }
        db.collection(collName, function (err, items) {
            if (err !== null) { db.close(); next(err); return; }
            items.remove({'_id':new mongo.ObjectID(req.params.item_id)}, true, function(err, count) {
                if (err !== null) { db.close(); next(err); return; }
                db.close();
                res.status(200).json({'count':count});
            }); // remove
        }); // collection
    }); // connect
};

